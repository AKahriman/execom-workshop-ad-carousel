## Design Patterns

The goal of this workshop is to raise awareness of powerful software design principals, design patterns and various tradeoffs each design decision brings. 

A somewhat simplified real world problem is presented with a skeleton codebase. This codebase is present on the `master` branch. 

Requirements are included as a PDF file along with some considerations. Please read through them carefully. Change requests are included as separate markdown files so you may view them only after completing all reqirements and change requests leading to them.

While the reader can go through the workshop alone, it is highly recommended to do it in a group up to 4 people. Groups come with diverse backgrounds and mindsets, thus, remember to stay open minded, because inclusion is what makes diversity worth while.

Even though the code skeleton is written in C#, core principles practiced in this workshop can be applied to any object oriented or functional language. Having said that, if you plan to work in C#, please make sure you have Visual Studio 2019 installed.

Proposed solution can be found on the `design-patterns-final` branch. Remember, this is likely one of many good solutions, hence, don't analyze it too much at the expense of experimenting yourself and arriving at something different and possibly better!  

For more questions you are welcome to contact akahriman or lkovac.
