One ad should be shown if it’s between 5 AM and 11 AM or it’s the weekend between 4PM and 9PM.

This implies being able to evaluate non trivial combinations of AND & OR logic operators.