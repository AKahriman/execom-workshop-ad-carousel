Advertisements should be shown based on a location rule. For example, an advertisement for McDonalds should be shown if the user is in the vicinity (e.g. < 1km from it).

Please merge the branch `design-patterns-change-request-3` into your working branach locally.